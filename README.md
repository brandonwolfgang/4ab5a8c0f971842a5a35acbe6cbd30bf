# Project information
This project was built and tested using Java.

    java version "1.8.0_91"
    Java(TM) SE Runtime Environment (build 1.8.0_91-b14)
    Java HotSpot(TM) 64-Bit Server VM (build 25.91-b14, mixed mode)

# Build Instructions

Before you do anything else, make sure to get the code:

    git clone git@gitlab.com:brandonwolfgang/4ab5a8c0f971842a5a35acbe6cbd30bf.git
    cd 4ab5a8c0f971842a5a35acbe6cbd30bf

## [Gradle](http://gradle.org/) Installation
You will need [Gradle](http://gradle.org/) to install dependencies and compile the code. If you do not have it installed follow the instructions below.

1. [Download](http://gradle.org/gradle-download/) the latest stable release.
2. [Install](https://docs.gradle.org/current/userguide/installation.html) using the Gradle documentation.
3. Test your installation by running `gradle -v` in your command line. You should see something like the following.  

```
------------------------------------------------------------
Gradle 2.14
------------------------------------------------------------

Build time:   2016-06-14 07:16:37 UTC
Revision:     cba5fea19f1e0c6a00cc904828a6ec4e11739abc

Groovy:       2.4.4
Ant:          Apache Ant(TM) version 1.9.6 compiled on June 29 2015
JVM:          1.8.0_91 (Oracle Corporation 25.91-b14)
OS:           Mac OS X 10.10.5 x86_64
```

## Build the project using Gradle

At this point you have all the code you need and a properly configured Gradle installation. 
Simply run `gradle build` in the command line and Gradle will use the `build.gradle` file to install dependencies and compile the source code. An executable `.jar` file is also created in `build/libs/`.

## Run the application

Use the command below to run the web application server:

    java -jar build/libs/bw-workorder-service-0.1.0.jar

[Spring Boot](http://projects.spring.io/spring-boot/) will start the application, initialize a local web server, and map the various endpoints.

## Test the endpoints

There are several ways to test the application. All URIs referenced in this section can be tested using any one of these methods.

### Command Line
Probably the simplest way to test is using [cURL](https://curl.haxx.se/docs/manpage.html) in the command line.  
Start by checking the work orders in the queue: `curl localhost:8080/work-orders/`. As you may suspect this will not return any results since we haven't added any work orders to the queue. To do that, run `curl -X POST localhost:8080/work-orders/add/1/<yyyyMMddHHmmss>/` and you will see the following output:

    {"dateString":"20160701200502","message":"Work order added on Fri Jul 01 20:05:02 EDT 2016 with ID 1.","status":"201 - Created"}

Now re-run the first command and you'll see a list containing the single element's ID `["1"]`. If there were more than work order in the queue, the list would be represented like so: `["1", "2", ..., n]`.

Try adding some other work orders.

### Browser

Basic `GET` requests can be performed simply in the browser's address bar, but `POST` requests will require using `curl` as above or [Postman](https://www.getpostman.com/) as shown below. Let's test by getting the top work order from the queue. Go to `http://localhost:8080/work-orders/top/` in your favorite browser. Assuming you've created at least one work order, the browser will display `{"ID":"1","Date Entered":"Fri Jul 01 21:49:43 EDT 2016","status":"200 - OK"}`.

### [Postman](https://www.getpostman.com/)

Postman is an awesome *free* REST client that has all sorts of great features. I won't get into them all here but figured I would point out the application in case you haven't used it before.

Once you've downloaded and installed Postman, the main screen has a `GET` request ready to go. Enter `localhost:8080/work-orders/` and a ranked list of work order IDs will be shown in the console.

## Available endpoints
| Endpoint | HTTP Method | Purpose | Example | Output |
|----------|---------|---------|---------|--------|
| `work-orders/`    |`GET`| **Returns a list of work order IDs**: This endpoint should return a list of IDs sorted in rank from highest to lowest. | `localhost:8080/work-orders/` | `["1","2","3","4"]` |
| `add/<id>/<yyyyMMddHHmmss>`    |`POST`| **Adds and ID to the queue**: This endpoint accepts two parameters, the ID to enqueue and an *optional datetime string* representing the time at which the ID was added to the queue. (The date/time is optional because it is automatically generated.) | `localhost:8080/add/1/` | `{"dateString":"20160701205302","message":"Work order added on Fri Jul 01 20:53:02 EDT 2016 with ID 5.","status":"201 - Created"}` |
| `remove/<id>/`    |`POST`| **Removes an ID from the queue**: This endpoint accepts a single parameter, the ID to remove. | `localhost:8080/work-orders/remove/1/` | `{"message":"Work order with ID 1 removed.","status":"200 - OK"}` |
| `top/`    |`GET`| **Returns highest ranked ID**: This endpoint should return the highest ranked ID and the time it was entered into the queue. | `localhost:8080/top/` | `{"ID":"1","Date Entered":"Fri Jul 01 21:49:43 EDT 2016","status":"200 - OK"}` |
| `position/<id>/`    |`GET`| **Returns the zero-based position of an ID in the queue**: This endpoint should accept one parameter, the ID to get the position of. It should return the position of the ID in the queue indexed from 0. | `localhost:8080/position/1/` | `{"position":"0","status":"200 - OK"}` |
| `average-wait-time/<yyyyMMddHHmmss>` |`GET`| **Returns the average wait time in seconds**: This endpoint accepts a single *optional* parameter, the current time (Thu Jun 30 01:47:52 EDT 2016), and should return the average (mean) number of seconds that each ID has been waiting in the queue. (If no time is provided it will be automatically generated.)| `localhost:8080/average-wait-time/` | `{"timeInQueue":"1013","status":"200 - OK"}` |


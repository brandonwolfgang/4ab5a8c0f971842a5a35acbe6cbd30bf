package com.brandonwolfgang.workorderqueue;

import java.text.SimpleDateFormat;
import java.util.Date;

public class WorkOrder implements Comparable<WorkOrder> {

    private final long id;
    private final Date date;

    public WorkOrder(long id, Date date) {
        this.id = id;
        this.date = date;
    }

    public long getId() {
        return id;
    }

    public Date getDate() {
        return date;
    }

    @Override
    public int compareTo(WorkOrder workOrder) {
        long workOrderId = workOrder.getId();
        Date workOrderDate = workOrder.getDate();

        if (this.getId() < workOrderId) {
            return -1;
        } else if (this.getId() > workOrderId) {
            return 1;
        } else if (this.getId() == workOrderId) {
            return this.getDate().compareTo(workOrderDate);
        }

        // Should not reach here
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof WorkOrder)) {
            return false;
        }

        WorkOrder workOrder = (WorkOrder) o;
        if (this.id == workOrder.getId()) {
            return true;
        } else {
            return false;
        }
    }
}
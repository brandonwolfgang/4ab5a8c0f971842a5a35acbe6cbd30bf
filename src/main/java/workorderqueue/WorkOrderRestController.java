package com.brandonwolfgang.workorderqueue;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/work-orders/", method = RequestMethod.GET, produces = "application/json")
public class WorkOrderRestController {

    private static final Comparator<WorkOrder> idComparator = Comparator.comparing(WorkOrder::getId);
    private static Queue<WorkOrder> workOrderPriorityQueue = new PriorityQueue<>(10, idComparator);
    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");

    @RequestMapping(method = RequestMethod.GET)
    public
    @ResponseBody
    ResponseEntity<?> getWorkOrderList() {

        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.OK;

        if (workOrderPriorityQueue.isEmpty()) {
            httpStatus = HttpStatus.BAD_REQUEST;
            Map<String, String> response = new HashMap<String, String>();
            response.put("message", "No work orders in queue.");
            response.put("status", httpStatus.toString() + " - " + httpStatus.getReasonPhrase());
            return new ResponseEntity<>(response, httpHeaders, httpStatus);
        }

        WorkOrder[] workOrders = workOrderPriorityQueue.toArray(new WorkOrder[0]);
        Arrays.sort(workOrders, idComparator);
        List<String> workOrderIdList = new ArrayList<String>();

        for (WorkOrder w : workOrders) {
            workOrderIdList.add(String.valueOf(w.getId()));
        }

        return new ResponseEntity<>(workOrderIdList, httpHeaders, httpStatus);
    }

    @RequestMapping(value = "/add/{userId}/{dateString}/", method = RequestMethod.POST)
    public
    @ResponseBody
    ResponseEntity<Map> addWorkOrder(@PathVariable String userId, @PathVariable String dateString) {

        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.CREATED;
        Map<String, String> response = new HashMap<String, String>();

        if (dateStringIsValid(dateString)) {
            Date date = getDateFromDateString(dateString);
            WorkOrder workOrder = new WorkOrder(Long.parseLong(userId), date);

            if (workOrderPriorityQueue.contains(workOrder)) {
                httpStatus = HttpStatus.BAD_REQUEST;
                response.put("status", httpStatus.toString() + " - " + httpStatus.getReasonPhrase());
                response.put("message", String.format("ID %s already exists in queue.", userId));
            } else {
                workOrderPriorityQueue.add(workOrder);
                response.put("status", httpStatus.toString() + " - " + httpStatus.getReasonPhrase());
                response.put("dateString", dateString);
                response.put("message", String.format("Work order added on %s with ID %s.",
                        workOrder.getDate().toString(), String.valueOf(workOrder.getId())));
            }
        } else {
            httpStatus = HttpStatus.BAD_REQUEST;
            response.put("status", httpStatus.toString() + " - " + httpStatus.getReasonPhrase());
            response.put("message", String.format("Date string %s is invalid. Proper format: '%s' (military hours).",
                    dateString, simpleDateFormat.toPattern()));
        }

        return new ResponseEntity<>(response, httpHeaders, httpStatus);
    }

    @RequestMapping(value = "/top/", method = RequestMethod.GET)
    public
    @ResponseBody
    ResponseEntity<Map> getTopIdFromQueue() {

        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.OK;
        Map<String, String> response = new HashMap<String, String>();

        if (workOrderPriorityQueue.isEmpty()) {
            httpStatus = HttpStatus.BAD_REQUEST;
            response.put("message", "No work orders in queue.");
            response.put("status", httpStatus.toString() + " - " + httpStatus.getReasonPhrase());
        } else {
            WorkOrder workOrder = workOrderPriorityQueue.poll();
            response.put("ID", String.valueOf(workOrder.getId()));
            response.put("Date Entered", workOrder.getDate().toString());
            response.put("status", httpStatus.toString() + " - " + httpStatus.getReasonPhrase());
        }

        return new ResponseEntity<>(response, httpHeaders, httpStatus);
    }

    @RequestMapping(value = "/remove/{userId}/", method = RequestMethod.POST)
    public
    @ResponseBody
    ResponseEntity<Map> removeWorkOrder(@PathVariable String userId) {

        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus;
        Map<String, String> response = new HashMap<String, String>();

        Date date = new Date();
        WorkOrder workOrder = new WorkOrder(Long.parseLong(userId), date);

        if (workOrderPriorityQueue.contains(workOrder)) {
            workOrderPriorityQueue.remove(workOrder);
            httpStatus = HttpStatus.OK;
            response.put("status", httpStatus.toString() + " - " + httpStatus.getReasonPhrase());
            response.put("message",
                    String.format("Work order with ID %s removed.", String.valueOf(workOrder.getId())));
        } else {
            httpStatus = HttpStatus.BAD_REQUEST;
            response.put("status", httpStatus.toString() + " - " + httpStatus.getReasonPhrase());
            response.put("message", String.format("No work orders with ID %s exist in queue.", userId));
        }

        return new ResponseEntity<>(response, httpHeaders, httpStatus);
    }

    @RequestMapping(value = "/position/{userId}/", method = RequestMethod.GET)
    public
    @ResponseBody
    ResponseEntity<Map> getPositionInQueue(@PathVariable String userId) {

        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.OK;
        Map<String, String> response = new HashMap<String, String>();

        if (workOrderPriorityQueue.isEmpty()) {
            httpStatus = HttpStatus.BAD_REQUEST;
            response.put("status", httpStatus.toString() + " - " + httpStatus.getReasonPhrase());
            response.put("message", "No work orders in queue.");
        } else {
            Date date = new Date();
            WorkOrder workOrder = new WorkOrder(Long.parseLong(userId), date);

            if (workOrderPriorityQueue.contains(workOrder)) {
                WorkOrder[] workOrders = workOrderPriorityQueue.toArray(new WorkOrder[0]);

                Arrays.sort(workOrders, idComparator);

                for (int i = 0; i < workOrders.length; i++) {
                    if (workOrders[i].getId() == Long.parseLong(userId)) {
                        String position = String.valueOf(i);
                        response.put("position", String.valueOf(i));
                        response.put("status", httpStatus.toString() + " - " + httpStatus.getReasonPhrase());
                    }
                }
            } else {
                httpStatus = HttpStatus.NOT_FOUND;
                response.put("message", String.format("No work orders with ID %s in queue.", userId));
                response.put("status", httpStatus.toString() + " - " + httpStatus.getReasonPhrase());
            }
        }

        return new ResponseEntity<>(response, httpHeaders, httpStatus);
    }

    @RequestMapping(value = "/average-wait-time/{dateString}/", method = RequestMethod.GET)
    public
    @ResponseBody
    ResponseEntity<Map> getAverageWaitTime(@PathVariable String dateString) {

        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.OK;

        Map<String, String> response = new HashMap<String, String>();

        if (workOrderPriorityQueue.isEmpty()) {
            httpStatus = HttpStatus.BAD_REQUEST;
            response.put("message", "No work orders in queue.");
            response.put("status", httpStatus.toString() + " - " + httpStatus.getReasonPhrase());
        } else {
            if (dateStringIsValid(dateString)) {
                long averageWaitTime = calculateAverageWaitTime(dateString);
                response.put("timeInQueue", String.valueOf(averageWaitTime));
                response.put("status", httpStatus.toString() + " - " + httpStatus.getReasonPhrase());
            } else {
                httpStatus = HttpStatus.BAD_REQUEST;
                response.put("status", httpStatus.toString() + " - " + httpStatus.getReasonPhrase());
                response.put("message", String.format("Date string %s is invalid. Proper format: '%s' (military hours).",
                        dateString, simpleDateFormat.toPattern()));
            }
        }

        return new ResponseEntity<>(response, httpHeaders, httpStatus);
    }

    private boolean dateStringIsValid(String dateString) {

        Date date = null;

        try {
            date = simpleDateFormat.parse(dateString);
            if (!dateString.equals(simpleDateFormat.format(date))) {
                date = null;
                return false;
            }
        } catch (ParseException ex) {
            date = null;
            return false;
        }

        return true;
    }

    private Date getDateFromDateString(String dateString) {

        Date date = null;

        try {
            date = simpleDateFormat.parse(dateString);
        } catch (ParseException ex) {
            date = null;
        }

        return date;
    }

    private long calculateAverageWaitTime(String dateString) {
        Date date = getDateFromDateString(dateString);
        long currentTime = date.getTime();
        long totalWaitTime = 0L;
        List<Long> waitTimes = new ArrayList<Long>();
        WorkOrder[] workOrders = workOrderPriorityQueue.toArray(new WorkOrder[0]);

        for (WorkOrder w : workOrders) {
            long waitTime = currentTime - w.getDate().getTime();
            waitTimes.add(waitTime);
        }

        for (long waitTime : waitTimes) {
            totalWaitTime += waitTime;
        }

        return (totalWaitTime / waitTimes.size()) / 1000;
    }
}
